import math
import random
import sys
import time

# Perform simulated annealing of processArray
def annealing(processArray):
    s = initConfig(processArray)
    sEval = objective(s, processArray)
    sBest = s[:]
    sBestEval = sEval

    t = initTemp(s, processArray)

    nbPerm = 0
    nbPermSucceed = 0
    nbTempLevel = 0

    while True:
        p = perturbation(s)
        pEval = objective(p, processArray)
        nbPerm += 1

        a = pEval - sEval
        if a < 0 or random.random() < math.exp(-a / t):
            s = p[:]
            if pEval < sBestEval:
                sBest = s[:]
                sBestEval = pEval
            sEval = pEval
            nbPermSucceed += 1

        if balanceThermo(nbPerm, nbPermSucceed, 100):
            if nbTempLevel >= 3:
                break
            else:
                t = decreaseTemp(t)
                if nbPermSucceed == 0:
                    nbTempLevel += 1
                else:
                    nbTempLevel = 0
                nbPerm = 0
                nbPermSucceed = 0
    return sBest

# Build initial solution
def initConfig(processArray):
    tmp = []
    for i in range(len(processArray[0])):
        tmp.append(i + 1)

    s = []
    while len(tmp) != 0:
        total = [0] * len(tmp)
        for j in range(len(tmp)):
            for i in range(len(processArray) - 1):
                for k in range(len(tmp)):
                    if k != j:
                        total[j] += processArray[i][tmp[j] - 1]
                    total[j] -= processArray[i + 1][tmp[j] - 1]
        for k in [i for i, j in enumerate(total) if j == min(total)]:
            s.append(tmp[k])
            tmp.pop(k)
    print(s)
    print(objective(s, processArray))
    return s

# Return the initial value of the temperature
def initTemp(s, processArray):
    sEval = objective(s, processArray)
    err = sEval
    for i in range(100):
        s = perturbation(s)
        pEval = objective(s, processArray)

        err += sEval - pEval
        sEval = pEval
    err = math.fabs(err) / 100
    return -err / math.log(0.8)

# Return an elementary perturbation of the solution
def perturbation(s):
    lenght = len(s)
    index1 = random.randint(0, lenght - 1)
    index2 = random.randint(0, lenght - 1)
    while index1 == index2:
        index2 = random.randint(0, lenght - 1)

    tmp = s[index1]
    s[index1] = s[index2]
    s[index2] = tmp

    return s

# Return the value of the objective function
def objective(s, processArray):
    nbMachines = len(processArray)
    nbJobs = len(processArray[0])

    reverseProcessArray = []
    for i in range(nbJobs):
        line = []
        for j in range(nbMachines):
            time = processArray[j][s[i] - 1]
            if i == 0 and j != 0:
                time += line[j - 1]
            elif i != 0 and j == 0:
                time += reverseProcessArray[i - 1][j]
            elif i != 0 and j != 0:
                time += max(reverseProcessArray[i - 1][j], line[j - 1])
            line.append(time)
        reverseProcessArray.append(line)

    return reverseProcessArray[nbJobs - 1][nbMachines - 1]

# Return a boolean that tells if the system is in Thermodynamic balance
def balanceThermo(nbPerm, nbPermSucceed, n):
    return nbPermSucceed >= 12 * n or nbPerm >= 100 * n

# Decrease temperature
def decreaseTemp(t):
    return t * 0.9

def perform(processArray):
    t1 = time.clock()
    solution = annealing(processArray)
    t2 = time.clock()
    print(solution)
    print(objective(solution, processArray))
    print("Time : " + repr(t2 - t1))

    return solution

def main():
    if len(sys.argv) == 3:
        processArray = []

        f = open(sys.argv[1])
        for line in f:
            processArray.append(line[:-1].split())
        f.close()

        processArray = processArray[3:]
        for i in range(len(processArray)):
            for j in range(len(processArray[i])):
                processArray[i][j] = int(processArray[i][j])

        sol = perform(processArray)

        f = open(sys.argv[2], 'w')
        for i in range(len(sol)):
            f.write(repr(sol[i]))
            if i != len(sol) - 1:
                f.write(" ")
        f.write("\n")
        f.close()


if __name__ == '__main__':
    main()
