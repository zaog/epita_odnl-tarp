import random

def parseTime(par_timeString):
    time_toks = par_timeString.split(':')
    if len(time_toks) != 2:
        return -1
    time_hour = int(time_toks[0])
    if time_hour < 0 or time_hour > 23:
        return -1
    time_mins = int(time_toks[1])
    if time_mins < 0 or time_mins > 59:
        return -1
    return time_hour * 60 + time_mins

class Network:
    idx = None
    idx_journey = None
    stations = None
    journeys = None

    def __init__(self):
        self.idx = []
        self.idx_journey = {}
        self.stations = []
        self.journeys = {}

    def addJourney(self, journey):
        if journey[0] not in self.idx:
            self.idx.append(journey[0])
            self.idx_journey[journey[0]] = [journey[1], journey[2], parseTime(journey[3]), parseTime(journey[4])]
            if journey[1] not in self.stations:
                self.stations.append(journey[1])
            if journey[1] not in self.journeys:
                self.journeys[journey[1]] = []
            if journey[2] not in self.stations:
                self.stations.append(journey[2])

            self.journeys[journey[1]].append([journey[0], journey[2], parseTime(journey[3]), parseTime(journey[4])])

        else:
            print("error")

    def getRandomSolution(self, idx_copy, result):
        while len(idx_copy) != 0:
            actual = []
            actual_day = []

            actual_id = idx_copy.pop(random.randint(0, len(idx_copy) - 1))
            actual_journey = self.idx_journey[actual_id]

            time_actual_day = actual_journey[3] - actual_journey[2]
            start_actual_day = actual_journey[2]
            number_days = 0

            actual_day.append(actual_id)
            actual_position = actual_journey[1]

            has_been_removed = None

            while number_days < 5:
                destination = self.getRandomDestination(actual_position)
                if destination == None:
                    break
                has_been_removed = None
                if destination[0] in idx_copy:
                    idx_copy.remove(destination[0])
                    has_been_removed = destination[0]
                if time_actual_day + destination[3] - destination[2] > 420 or destination[3] - start_actual_day > 600 or ((destination[3] - start_actual_day < 0) and (1440 + (destination[3] - start_actual_day) > 600)):
                    time_actual_day = destination[3] - destination[2]
                    start_actual_day = destination[2]
                    actual.append(actual_day)
                    actual_day = []
                    actual_day.append(destination[0])
                    number_days += 1
                else:
                    time_actual_day += destination[3] - destination[2]
                    actual_day.append(destination[0])
                actual_position = destination[1]
            
            if has_been_removed != None:
                idx_copy.append(has_been_removed)

            result.append(actual)

        return result

    def getRandomDestination(self, actual_position):
        if actual_position not in self.journeys.keys():
            return None
        return self.journeys[actual_position][random.randint(0, len(self.journeys[actual_position]) - 1)]

    def getMissingIdx(self, solution):
        result = []

        journeys = set()
        for worker in solution:
            for day in worker:
                for journey in day:
                    journeys.add(journey)

        result = [journey for journey in self.idx if journey not in journeys]
        return result

    def printNetwork(self):
        print(self.idx)
        print(self.stations)
        print(self.journeys)

