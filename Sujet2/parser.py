from network import *

def parse(filename):
    f = open(filename)

    network = Network()

    line_index = 1
    for line in f:
        res_add = network.addJourney(line.split()[:])
        if res_add != None:
            print('Parsing Error: Line ' + str(line_index) + ': ' + res_add)
            break
        line_index = line_index + 1
    f.close()
    return network
