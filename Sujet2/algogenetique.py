import math
import random
import sys
import time

from parser import *
from network import *

def genetic(network, factor, generation):
    print("Initiating population...")
    population = initPopulation(network, len(network.idx) * factor)
    bestInd = population[0]
    bestIndPerf = perf(bestInd)

    perfs = []
    for ind in population:
        p = perf(ind)
        perfs.append(p)
        if bestIndPerf < p:
            bestInd = ind
            bestIndPerf = p

#    return

    print("Starting algorithm...")
    n = 0
    while (n < generation):
        print("Generation: " + str(n))
        population.sort(key = objective)
        parents = selectReproduction(population)
        children = crossbreeding(parents, network)

        for i in range(len(children)):
            children[i] = mutation(children[i][:], network)

        population = children[:]
        for i in range(len(population)):
            p = perf(population[i])
            perfs[i] = p
            if bestIndPerf < p:
                # print('ok')
                bestInd = population[i]
                bestIndPerf = p

        n = n + 1

    return bestInd

# Initialize population
def initPopulation(network, n):
    population = []
    for i in range(0, n):
        population.append(network.getRandomSolution(network.idx[:], []))

    return population

# Perf function
def perf(ind):
    return 1.0 / objective(ind)

# Objective function
def objective(ind):
    return len(ind)

# Function for select the individual to reproduct
def selectReproduction(population):
    perfs = [perf(ind) for ind in population]
    parents = []

    while len(population) > 0:
        couple = (population.pop(random.randint(0, len(population) - 1)), population.pop(random.randint(0, len(population) - 1)))
        parents.append(couple)

    return parents

def numberJourneysForEmployee(employee):
    length = 0
    for d in employee:
        length += len(d)

    return length

def numberNewJourneysForEmployee(employee, missingIdx):
    length = 0
    for day in employee:
        for journey in day:
            if journey in missingIdx:
                length += 1
                missingIdx.remove(journey)

    return length

# Perform crossbreeding on the selected
def crossbreeding(parents, network):
    childrens = []    

    for mother,father in parents:
        parents_mix = mother + father
        parents_mix.sort(key = numberJourneysForEmployee)
        children = []
        missingIdx = network.idx[:]


        for employee in parents_mix:
            if numberNewJourneysForEmployee(employee, missingIdx) > int(len(network.idx) / 5):
                children.append(employee)
                missingIdx = network.getMissingIdx(children)

        children_missing_idx = network.getMissingIdx(children)
        children1 = network.getRandomSolution(children_missing_idx[:], children[:])
        children2 = network.getRandomSolution(children_missing_idx[:], children[:])

        childrens.append(children1)
        childrens.append(children2)

    return childrens

# Perform mutation on individual
def mutation(ind, network):
    ind.pop(random.randint(0, len(ind) - 1))

    missing_idx = network.getMissingIdx(ind)
    ind = network.getRandomSolution(missing_idx, ind)

    return ind

def main(argc, argv):
    if argc == 5:
        network = parse(argv[1])

        t1 = time.clock()
        sol = genetic(network, int(argv[3]), int(argv[4]))
        t2 = time.clock()
        print("\nTime: " + str(t2 - t1))

        f = open(argv[2], 'w')
        for i in range(len(sol)):
            for j in range(len(sol[i])):
                f.write(repr(i + 1) + " " + repr(j + 1))
                for k in range(len(sol[i][j])):
                    f.write(" " + sol[i][j][k])
                f.write("\n")
        f.close()

        # network.getRandomSolution(network.idx[:], []):

if __name__ == '__main__':
    main(len(sys.argv), sys.argv)
